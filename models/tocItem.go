package models

import (
	"fmt"
	"io"

	"github.com/charmbracelet/bubbles/list"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type TOCItem struct {
	label        string
	nestingLevel int
	content      string
}
type TOCItemDelegate struct{}

var (
	titleStyle        = lipgloss.NewStyle().MarginLeft(2)
	itemStyle         = lipgloss.NewStyle().PaddingLeft(4)
	selectedItemStyle = lipgloss.NewStyle().PaddingLeft(2).Foreground(lipgloss.Color("69"))
	paginationStyle   = list.DefaultStyles().PaginationStyle.PaddingLeft(4)
	quitTextStyle     = lipgloss.NewStyle().Margin(1, 0, 2, 4)
)

func (d TOCItemDelegate) Height() int                               { return 1 }
func (d TOCItemDelegate) Spacing() int                              { return 0 }
func (d TOCItemDelegate) Update(msg tea.Msg, m *list.Model) tea.Cmd { return nil }
func (d TOCItemDelegate) Render(w io.Writer, m list.Model, index int, listItem list.Item) {
	i, ok := listItem.(TOCItem)
	if !ok {
		return
	}

	indentation := ""
	for j := 0; j < i.nestingLevel; j++ {
		indentation = indentation + "  "
	}

	str := fmt.Sprintf(indentation+"%s", i.label)

	fn := itemStyle.Render
	if index == m.Index() {
		fn = func(s string) string {
			return selectedItemStyle.Render("> " + s)
		}
	}

	fmt.Fprint(w, fn(str))
}

func (TOCItem) FilterValue() string {
	return ""
}
