// Package contains Custom BubbleTea models.
package models

import (
	"adamszkolny/handbook/mdReader"
	"fmt"

	"github.com/charmbracelet/bubbles/list"
	"github.com/charmbracelet/bubbles/viewport"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
)

type handbookView int

const (
	tableOfContents handbookView = iota
	contentWindow   handbookView = iota
)

const (
	defaultTOCHeight     = 12
	defaultTOCWidth      = 20
	defaultContentHeight = 40
	defaultContentWidth  = 80
)

var (
	tocStyle = lipgloss.NewStyle().
			Align(lipgloss.Center, lipgloss.Center).
			BorderStyle(lipgloss.NormalBorder()).
			BorderForeground(lipgloss.Color("236")).
			PaddingRight(2)
	contentWindowStyle = lipgloss.NewStyle().
				Align(lipgloss.Center, lipgloss.Center).
				BorderStyle(lipgloss.DoubleBorder()).
				BorderForeground(lipgloss.Color("236"))
	focusedTOCStyle = lipgloss.NewStyle().
			Align(lipgloss.Center, lipgloss.Center).
			BorderStyle(lipgloss.NormalBorder()).
			BorderForeground(lipgloss.Color("69")).
			PaddingRight(2)
	focusedContentWindowStyle = lipgloss.NewStyle().
					Align(lipgloss.Center, lipgloss.Center).
					BorderStyle(lipgloss.DoubleBorder()).
					BorderForeground(lipgloss.Color("69"))
	helpStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("241"))
)

type MainModel struct {
	focusedView    handbookView
	TableOfContent list.Model
	Window         viewport.Model
	quitting       bool
	output         string
	modelsMap      map[handbookView]tea.Model
}

func (m MainModel) Init() tea.Cmd {
	return nil
}

// Update main model and pass bubble tea message to nested models
func (m MainModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd
	var cmds []tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		case "tab":
			if m.focusedView == tableOfContents {
				m.focusedView = contentWindow
			} else {
				m.focusedView = tableOfContents
			}
		case "enter":
			if m.focusedView == tableOfContents {
				viewportContent := m.TableOfContent.SelectedItem().(TOCItem).content
				renderedContent, _ := viewportRenderer.Render(viewportContent)
				m.Window.SetContent(renderedContent)
			}
		}
		switch m.focusedView {
		case tableOfContents:
			m.TableOfContent, cmd = m.TableOfContent.Update(msg)
			cmds = append(cmds, cmd)
		default:
			m.Window, cmd = m.Window.Update(msg)
			cmds = append(cmds, cmd)
		}
	}

	return m, tea.Batch(cmds...)
}

// Return all views string to render in terminal
func (m MainModel) View() string {
	var s string

	if m.focusedView == tableOfContents {
		s += lipgloss.JoinHorizontal(lipgloss.Top, focusedTOCStyle.Render(fmt.Sprintf("%s", m.TableOfContent.View())), contentWindowStyle.Render(m.Window.View()))
	} else {
		s += lipgloss.JoinHorizontal(lipgloss.Top, tocStyle.Render(fmt.Sprintf("%s", m.TableOfContent.View())), focusedContentWindowStyle.Render(m.Window.View()))
	}

	s += helpStyle.Render(fmt.Sprintf("\n ↑/k up • ↓/j down • q quit • enter read • tab focus next"))
	return s
}

func CreateTableOfContent(chapters []mdReader.Chapter, tocName string) list.Model {

	items := []list.Item{}
	for _, chapter := range chapters {
		items = append(items, TOCItem{chapter.Title, 0, chapter.Content})
	}

	l := list.New(items, TOCItemDelegate{}, defaultTOCWidth, defaultTOCHeight)
	l.Title = tocName
	l.SetShowStatusBar(false)
	l.SetFilteringEnabled(false)
	l.SetShowHelp(false)
	l.Styles.Title = titleStyle
	l.Styles.PaginationStyle = paginationStyle

	return l
}
