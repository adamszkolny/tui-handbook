// Package provides method for reading markdown files in the directory
// and returning them for further use.
package mdReader

import (
	"os"
	"path/filepath"
	"strings"
)

// Text with Content prefereably written with markdown
// and Title.
type Chapter struct {
	Title   string
	Content string
}

// Reads all files with .md extension in the directory, each read successfull
// file is returned as Chapter with its name as Chapter Title (without extension)
// and content as Content.
func ReadAllMdFiles(directory string) ([]Chapter, error) {
	dirEntries, err := os.ReadDir(directory)
	var chapters []Chapter
	for _, entry := range dirEntries {
		if strings.HasSuffix(entry.Name(), ".md") {
			fileContent, _ := os.ReadFile(filepath.Join(directory, entry.Name()))
			chapter := Chapter{
				strings.TrimSuffix(entry.Name(), ".md"),
				string(fileContent),
			}
			chapters = append(chapters, chapter)
		}
	}

	return chapters, err
}
