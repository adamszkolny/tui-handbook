package models

import (
	"github.com/charmbracelet/bubbles/viewport"
	"github.com/charmbracelet/glamour"
)

var (
	viewportRenderer, err = glamour.NewTermRenderer(
		glamour.WithAutoStyle(),
		glamour.WithWordWrap(defaultContentWidth),
	)
)

func CreateViewport(w, h int) (viewport.Model, error) {
	vp := viewport.New(w, h)

	if err != nil {
		return vp, err
	}

	str, err := viewportRenderer.Render("Welcome to Handbook")
	if err != nil {
		return vp, err
	}

	vp.SetContent(str)

	return vp, nil
}
