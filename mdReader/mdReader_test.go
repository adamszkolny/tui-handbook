package mdReader

import "testing"

func TestMdReader(t *testing.T) {
	t.Run("reading directory with md files", func(t *testing.T) {
		chapters, err := ReadAllMdFiles("../book")

		if err != nil {
			t.Errorf("error thrown when reading chapters")
		}

		if len(chapters) <= 0 {
			t.Errorf("chapters weren't locaded from files")
		}
	})
}
