# Cleric

    Clerics are adventurers who have sworn to serve a deity. They are trained for battle and channel the power of their deity.

## Combat

Clerics can use all types of armour. Strict holy doctrine prevents clerics’ use of weapons that have a sharp, cutting edge or stabbing point. They may use the following weapons: club, mace, sling, staff, war hammer.

## Divine Magic

Holy symbol: A cleric must carry a holy symbol (see Adventuring Gear).

Deity disfavour: Clerics must be faithful to the tenets of their alignment, clergy, and religion. Clerics who fall from favour with their deity may incur penalties.

Magical research: A cleric of any level may spend time and money on magical research. This allows them to create new spells or other magical effects associated with their deity. When a cleric reaches 9th level, they are also able to create magic items.

Spell casting: Once a cleric has proven their faith (from 2nd level), the character may pray to receive spells. The power and number of spells available to a cleric are determined by the character’s experience level. The list of spells available to clerics is found in Cleric Spells.

Using magic items: As spell casters, clerics can use magic scrolls of spells on their spell list. They can also use items that may only be used by divine spell casters (e.g. some magic staves).

## Turning the Undead

Clerics can invoke the power of their deity to repel undead monsters encountered. To turn the undead, the player rolls 2d6. The referee then consults the table below, comparing the roll against the HD of the type of undead monsters targeted.

### Successful Turning
If the turning attempt succeeds, the player must roll 2d6 to determine the number of HD affected (turned or destroyed).

Turned undead: Will leave the area, if possible, and will not harm or make contact with the cleric.

Destroyed undead (result of D): Are instantly and permanently annihilated.

Excess: Rolled Hit Dice that are not sufficient to affect a monster are wasted.

Minimum effect: At least one undead monster will always be affected on a successful turning.

Mixed groups: If turn undead is used against a mixed group of undead monsters of different types, those with the lowest HD are affected first.

### Turning the Undead

Hit Dice of Monster Type†
|Level| 1 | 2 | 2* | 3 | 4 | 5 | 6 | 7-9 | 
|---|---|---|---|---|---|---|---|---|
| 1 | 7 | 9 | 11 |  - |  - |  - | - |  - | 
| 2 | T | 7 |  9 | 11 |  - |  - | - |  - | 
| 3 | T | T |  7 |  9 | 11 |  - | - |  - | 
| 4 | D | T |  T |  7 |  9 | 11 | - |  - | 

* 2 HD monsters with a special ability (i.e. with an asterisk next to their HD rating, in the monster description).

† At the referee’s option, the table may be expanded to include more powerful types of undead monsters.