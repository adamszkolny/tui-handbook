package main

import (
	"adamszkolny/handbook/mdReader"
	"adamszkolny/handbook/models"

	"flag"
	"fmt"
	"os"

	tea "github.com/charmbracelet/bubbletea"
)

func main() {

	bookPath := flag.String("b", "", "Path to the folder containing markdown files.")
	flag.Parse()

	chapters, err := mdReader.ReadAllMdFiles(*bookPath)

	if err != nil {
		fmt.Println("Error reading files: "+*bookPath, err)
		os.Exit(1)
	}

	vp, _ := models.CreateViewport(120, 20)
	m := models.MainModel{TableOfContent: models.CreateTableOfContent(chapters, *bookPath), Window: vp}
	program := tea.NewProgram(m)

	if err = program.Start(); err != nil {
		fmt.Println("Error running rendering program:", err)
		os.Exit(1)
	}
}
