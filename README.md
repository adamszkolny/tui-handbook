# TUI Handbook

Simple program to read markdown files inside the terminal. 

To use, pass path to directory containing markdown files as a parameter to flag -b.

![Handbook Demo](https://gitlab.com/adamszkolny/tui-handbook/-/raw/main/demo.gif)
